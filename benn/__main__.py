#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2017 by Alexis Pietak & Cecil Curry
# See "LICENSE" for further details.

'''
Main entry point of this application's command line interface (CLI).

This submodule is a thin wrapper intended to be:

* Indirectly imported and run from external entry point scripts installed by
  setuptools (e.g., the ``benn`` command).
* Directly imported and run from the command line (e.g., via
  ``python -m benn.cli``).
'''

# ....................{ IMPORTS                            }....................
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING: To raise human-readable exceptions on missing mandatory dependencies,
# this module may import *ONLY* from standard Python packages and
# application-specific packages importing *ONLY* from standard Python packages.
# By definition, this excludes all third-party packages and most
# application-specific packages.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

import sys
from benn import bennmetadata
from benn.bennmetadata import NAME
from benn.guimetadeps import BETSE_VERSION_REQUIRED_MIN

# ....................{ EXCEPTIONS                         }....................
class _BetseNotFoundException(Exception):
    '''
    Exception raised on detecting BETSE (i.e., this application's core mandatory
    dependency) to be unimportable under the active Python interpreter.
    '''

    pass

# ....................{ MAIN                               }....................
def main(arg_list: list = None) -> int:
    '''
    Run this application's command-line interface (CLI) with the passed
    arguments if non-``None`` *or* with the arguments passed on the command line
    (i.e., :attr:`sys.argv`) otherwise.

    This function is provided as a convenience to callers requiring procedural
    functions rather than conventional methods (e.g., :mod:`setuptools`).

    Parameters
    ----------
    arg_list : list
        List of zero or more arguments to pass to this interface. Defaults to
        ``None``, in which case arguments passed on the command line (i.e.,
        :attr:`sys.argv`) will be used instead.

    Returns
    ----------
    int
        Exit status of this interface and hence this process as an unsigned byte
        (i.e., integer in the range ``[0, 255]``).
    '''

    # If BETSE is unsatisfied, raise an exception. Validate this *BEFORE*
    # attempting to import from this application's package tree, most of which
    # assumes BETSE to be satisfied.
    _die_unless_betse()

    # Import from this application's package tree *AFTER* validating BETSE.
    from benn.cli.benncli import BennCLI

    # Run this application's CLI and return the exit status of doing so.
    return BennCLI().run(arg_list)

# ....................{ EXCEPTIONS                         }....................
def _die_unless_betse() -> None:
    '''
    Raise an exception unless BETSE, the principal mandatory dependency of this
    application, is **satisfied** (i.e., both importable and of a version
    greater than or equal to that required by this application).

    Raises
    ----------
    _BetseNotFoundException
        If BETSE is unsatisfied (i.e., either unimportable or of a version
        less than that required by this application).
    '''

    # Attempt to import BETSE.
    try:
        import betse
    # If BETSE is unimportable, chain this low-level "ImportError" exception
    # into a higher-level application-specific exception.
    except ImportError as import_error:
        raise _BetseNotFoundException(
            'Mandatory dependency "betse" unimportable.') from import_error
    # Else, BETSE is importable.

    # Minimum version of BETSE required by this application as a
    # machine-readable tuple of integers. Since this tuple is only required once
    # (namely, here), this tuple is *NOT* persisted as a "metadata" global.
    BETSE_VERSION_REQUIRED_MIN_PARTS = (
        bennmetadata._convert_version_str_to_tuple(BETSE_VERSION_REQUIRED_MIN))

    # If the current version of BETSE is insufficient, raise an exception.
    if betse.__version_info__ < BETSE_VERSION_REQUIRED_MIN_PARTS:
        raise _BetseNotFoundException(
            '{} requires at least BETSE {}, '
            'but only BETSE {} is currently installed.'.format(
                NAME, BETSE_VERSION_REQUIRED_MIN, betse.__version__))

    # Purely for testing purposes.
    # raise _BetseNotFoundException(
    #     title=EXCEPTION_TITLE,
    #     synopsis='BETSE not found.',
    #     exegesis='Python package "betse" not importable.',
    # )

# ....................{ MAIN                               }....................
# If this module is imported from the command line, run this application's CLI;
# else, noop. For POSIX compliance, the exit status returned by this function
# is propagated to the caller as this script's exit status.
if __name__ == '__main__':
    sys.exit(main())
