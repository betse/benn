#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2014-2017 by Santosh Manicka, Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.

'''
Collection of the absolute paths of numerous critical files and directories
describing the structure of this application on the local filesystem.

These are intended for consumption by both this application and downstream
reverse dependencies of this application. For portability, these paths are
initialized in a system-aware manner guaranteed to be sane under insane
installation environments -- including PyInstaller-frozen executables and
:mod:`setuptools`-installed script wrappers.
'''

# ....................{ IMPORTS                            }....................
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING: To raise human-readable exceptions on missing mandatory dependencies,
# the top-level of this module may import *ONLY* from packages guaranteed to
# exist at installation time (i.e., standard Python packages). Likewise, to
# avoid circular import dependencies, the top-level of this module should avoid
# importing application packages except where explicitly required.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

from benn import bennmetadata
from betse import pathtree as betse_pathtree
from betse.util.path import dirs, pathnames
from betse.util.type.call.memoizers import callable_cached

# ....................{ GETTERS ~ dir : data               }....................
@callable_cached
def get_data_dirname() -> str:
    '''
    Absolute path of this application's top-level data directory if found *or*
    raise an exception otherwise (i.e., if this directory is *not* found).

    This directory contains application-internal resources (e.g., media files)
    required at application runtime.
    '''

    # Avoid circular import dependencies.
    import benn

    # Absolute path of this directory.
    data_dirname = pathnames.get_app_pathname(package=benn, pathname='data')

    # If this directory is not found, raise an exception.
    dirs.die_unless_dir(data_dirname)

    # Return the absolute path of this directory.
    return data_dirname

# ....................{ GETTERS ~ dir : dot                }....................
@callable_cached
def get_dot_dirname() -> str:
    '''
    Absolute path of this application's top-level dot directory in the home
    directory of the current user, silently creating this directory if *not*
    already found.

    This directory contains user-specific files (e.g., generated Python modules)
    both read from and written to at application runtime. These are typically
    plaintext files consumable by external users and third-party utilities.

    For tidiness, this directory currently resides under BETSE's dot directory
    (e.g., ``~/.betse/benn`` under Linux).
    '''

    # Create this directory if needed and return its dirname.
    return dirs.join_and_make_unless_dir(
        betse_pathtree.get_dot_dirname(), bennmetadata.SCRIPT_BASENAME)
