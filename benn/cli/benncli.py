#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2014-2017 by Alexis Pietak & Cecil Curry
# See "LICENSE" for further details.

'''
Concrete subclasses defining this application's command line interface (CLI).
'''

# ....................{ IMPORTS                            }....................
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING: To raise human-readable exceptions on application startup, the
# top-level of this module may import *ONLY* from submodules guaranteed to:
# * Exist, including standard Python and application modules, including both
#   BENN and BETSE modules.
# * Never raise exceptions on importation (e.g., due to module-level logic).
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

from benn import bennignition, bennmetadata
from benn.cli import benncliinfo
from betse.cli.cliabc import CLIABC
from betse.cli.cliopt import CLIOptionArgStr
from betse.util.io.log import logs
from betse.util.type.types import type_check, MappingType

# ....................{ SUBCLASS                           }....................
class BennCLI(CLIABC):
    '''
    Command line interface (CLI) for this application.
    '''

    # ..................{ INITIALIZERS                       }..................
    def __init__(self):

        # Initialize our superclass.
        super().__init__()

    # ..................{ SUPERCLASS ~ header                }..................
    def _show_header(self) -> None:

        benncliinfo.log_header()

    # ..................{ SUPERCLASS ~ properties            }..................
    @property
    def _arg_parser_top_kwargs(self) -> MappingType:

        return {
            # Human-readable multi-sentence application description.
            'description': bennmetadata.DESCRIPTION,
        }

    # ..................{ SUPERCLASS ~ options               }..................
    def _make_options_top(self) -> tuple:

        # Tuple of all default top-level options.
        options_top = super()._make_options_top()

        # Return a tuple extending this tuple with subclass-specific options.
        # For simplicity, this tuple is returned unmodified for now.
        return options_top


    def _parse_options_top(self) -> None:

        # Parse all default top-level options.
        super()._parse_options_top()

    # ..................{ SUPERCLASS ~ methods               }..................
    def _ignite_app(self) -> None:

        # (Re-)initialize both BENN and BETSE.
        bennignition.reinit()


    def _do(self) -> object:
        '''
        Implement this command-line interface (CLI) by running the corresponding
        graphical user interface (GUI), returning this interface to be memory
        profiled when the ``--profile-type=size`` CLI option is passed.
        '''

        # Defer imports *NOT* guaranteed to exist at this module's top-level.
        from betsee.gui.guimain import BetseeGUI

        # Application GUI.
        #
        # For safety, this GUI is scoped to a local rather than instance or
        # global variable, ensuring this GUI is destroyed before the root Qt
        # application widget containing this GUI.
        app_gui = BetseeGUI(sim_conf_filename=self._sim_conf_filename)

        # Run this GUI's event loop and display this GUI, propagating the
        # returned exit status as this application's exit status.
        self._exit_status = app_gui.run()

        # Return this GUI for optional profiling purposes.
        return app_gui


    @type_check
    def _handle_exception(self, exception: Exception) -> None:

        # Defer to superclass handling, which typically logs this exception.
        super()._handle_exception(exception)

        # Additionally attempt to...
        try:
            # Import PySide2.
            from betsee.util.io import guierr

            # Display a PySide2-based message box displaying this exception.
            guierr.show_exception(exception)
        # If PySide2 or any other module indirectly imported above is
        # unimportable, print this exception message but otherwise ignore this
        # exception. Why? Because we have more significant fish to fry.
        except ImportError as import_error:
            logs.log_error(str(import_error))
