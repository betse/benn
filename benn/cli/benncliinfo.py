#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2014-2017 by Alexis Pietak & Cecil Curry
# See "LICENSE" for further details.

'''
Implementation of the ``info`` subcommand for this command line interface (CLI).
'''

#FIXME; For aesthetics, convert to yppy-style "cli.memory_table" output.

# ....................{ IMPORTS                            }....................
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING: To avoid non-trivial delays on importing this module, this module
# should import *ONLY* from modules and packages whose importation is unlikely
# to incur such delays. This includes all standard Python packages and all BETSE
# packages required by the log_info_header() function, which is called
# sufficiently early in application startup as to render these imports
# effectively mandatory.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

from betse import metadata as betse_metadata
from betse.cli import cliinfo as betse_cliinfo
from betse.util.io.log import logs
from betsee import guimetadata as bennmetadata

# ..................{ LOGGERS                                }..................
def log_header() -> None:
    '''
    Log a single-line human-readable sentence synopsizing the state of the
    current application (e.g., name, codename, version).
    '''

    logs.log_info(
        'Welcome to <<'
        '{benn_name} {benn_version} | '
        '{betse_name} {betse_version} | '
        '{betse_codename}'
        '>>.'.format(
            benn_name=bennmetadata.NAME,
            benn_version=bennmetadata.VERSION,
            betse_name=betse_metadata.NAME,
            betse_version=betse_metadata.VERSION,
            betse_codename=betse_metadata.CODENAME,
        ))


def log_info() -> None:
    '''
    Log all metadata required by the ``info`` subcommand.
    '''

    # Defer to the BETSE implementation of this function, for simplicity.
    betse_cliinfo.log_info()
