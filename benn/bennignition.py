#!/usr/bin/env python3
# --------------------( LICENSE                            )--------------------
# Copyright 2014-2017 by Santosh Manicka, Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.

'''
High-level application initialization common to both the CLI and GUI.
'''

# ....................{ IMPORTS                            }....................
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# WARNING: To defer heavyweight and possibly circular imports, the top-level of
# this module may import *ONLY* from standard Python packages. All imports from
# application and third-party packages should be deferred to their point of use.
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# ....................{ INITIALIZERS                       }....................
def reinit() -> None:
    '''
    (Re)-initialize the current application with sane defaults.

    Specifically, this function (in order):

    #. Initializes all lower-level BETSE logic by calling the
       :func:`betse.ignition.init` function.
    #. Validates but does *not* initialize mandatory third-party dependencies of
       this application, which must be initialized independently by the
       :func:`benn.lib.bennlibs.init` function.
    '''

    # Defer heavyweight and possibly circular imports.
    from benn.lib import bennlibs
    from betse import ignition as betse_ignition

    # Initialize all lower-level BETSE logic *BEFORE* any higher-level BENN
    # logic assuming the former to "be sane." See the
    # betse.climain.BetseCLI._ignite_app() method for details on why the
    # betse_ignition.reinit() rather than betse_ignition.init() function is
    # called here.
    betse_ignition.reinit()

    # Validate mandatory dependencies. Avoid initializing these dependencies
    # here (e.g., by calling libs.init()), which requires the logging
    # configuration to have been finalized (e.g., by parsing CLI options), which
    # has yet to occur this early in the application lifecycle.
    bennlibs.die_unless_runtime_mandatory_all()
