.. # ------------------( BADGES                             )------------------
.. image::  https://gitlab.com/betse/benn/badges/master/build.svg
   :target: https://gitlab.com/betse/benn/pipelines
   :alt: Linux Build Status

.. # FIXME: Uncomment after creating a Windows-specific AppVeyor BENN project.
.. # .. image::  https://ci.appveyor.com/api/projects/status/mow7y8k3vpfu30c6/branch/master?svg=true
.. #    :target: https://ci.appveyor.com/project/betse/betse/branch/master
.. #    :alt: Windows Build Status

.. # ------------------( SYNOPSIS                           )------------------

======
BENN
======

**BENN** (**B**\ io\ **E**\ lectric **N**\ eural **N**\ etworker) is an
open-source cross-platform biological neural network simulator based on
BETSE_, a `finite volume`_ simulator for 2D computational multiphysics problems
in the life sciences. BENN accurately reproduces a variety of biophysical
(rather than artificial) networks, including those comprised of
electrochemically communicating somatic cells.

Like BETSE_, BENN is `portably implemented <codebase_>`__ in pure `Python 3
<Python_>`__, `continuously stress-tested <testing_>`__ with GitLab-CI_ **×**
Appveyor_ **+** py.test_, and `permissively distributed <License_>`__ under the
`BSD 2-clause license`_.

BENN and BETSE_ are both associated with the `Paul Allen Discovery Center`_ at
`Tufts University`_ and supported by a `Paul Allen Discovery Center award`_ from
the `Paul G. Allen Frontiers Group`_.

.. # ------------------( TABLE OF CONTENTS                  )------------------
.. # Blank line. By default, Docutils appears to only separate the subsequent
.. # table of contents heading from the prior paragraph by less than a single
.. # blank line, hampering this table's readability and aesthetic comeliness.

|

.. # Table of contents, excluding the above document heading. While the
.. # official reStructuredText documentation suggests that a language-specific
.. # heading will automatically prepend this table, this does *NOT* appear to
.. # be the case. Instead, this heading must be explicitly declared.

.. contents:: **Contents**
   :local:

.. # ------------------( DESCRIPTION                        )------------------

Installation
============

.. Note::
   BENN is pre-release software under active development. No packages
   automating installation of either BENN itself *or* BENN dependencies are
   currently provided. In particular, there currently exist no:

   - Platform-agnostic BENN packages (e.g., Anaconda_, PyPI_).
   - Platform-specific BENN packages (e.g., macOS_ Homebrew_, Ubuntu_ PPA_).

BENN is installable as follows:

#. Install the `unstable live version <BETSE live_>`__ of BETSE_.
#. Open a **terminal.** [#terminal]_
#. Clone the ``master`` branch of this repository.

   .. code:: bash

      git clone https://gitlab.com/betse/benn.git

#. **Install BENN.**

   .. code:: bash

      cd benn
      sudo python3 setup.py install


.. [#terminal]
   To open a `POSIX`_\ -compatible terminal under:

   - **Windows:**

     #. Install `Bash on Ubuntu on Windows`_.
     #. Open the *Start* menu.
     #. Open *Bash on Ubuntu on Windows*.

   - **macOS:**

     #. Open the *Finder*.
     #. Open the *Applications* folder.
     #. Open the *Utilities* folder.
     #. Open *Terminal.app*.

   - **Ubuntu Linux:**

     #. Type ``Ctrl``\ +\ ``Alt``\ +\ ``t``.

.. [#pyside2_install]
   Like BENN, PySide2_ is pre-release software under active development.
   Unlike BENN, packages automating installation of both PySide2_ itself and
   PySide2_ dependencies (e.g., Qt_) *are* available for various platforms –
   including:

   + `Arch Linux`_ via the `official PySide2 installation instructions
     <PySide2 installation_>`__.
   + `Gentoo Linux`_ via the `official PySide2 installation instructions
     <PySide2 installation_>`__.
   + Most other platforms (e.g., `Ubuntu Linux`_, CentOS_, macOS_, Windows) via
     the `unofficial PySide2 wheels <PySide2 wheels_>`__.
   + The platform-agnostic `Anaconda`_ and `Miniconda`_ Python distributions
     via the `unofficial PySide2 conda-forge feedstock <PySide2 feedstock_>`__.

License
=======

BETSE is open-source software `released <LICENSE>`__ under the permissive `BSD
2-clause license`_.

.. # FIXME: Consider adding a reference here to your first paper when published.
.. # For example, the BETSE reference list currently reads:

.. # Reference
.. # =========
.. # 
.. # BETSE is formally described in our `introductory paper <2016 article_>`__.
.. # Third-party papers, theses, and other texts leveraging BETSE should (ideally)
.. # cite the following:
.. # 
.. #     `Pietak, Alexis`_ and `Levin, Michael`_, 2016. |2016 article name|_
.. #     |2016 journal name|_ *4*\ (55). :sup:`DOI: 10.3389/fbioe.2016.00055`
.. # 
.. # Subsequent papers expanding the BETSE architecture with additional theory,
.. # experimental results, and comparative metrics include:
.. # 
.. #     `Pietak, Alexis`_ and `Levin, Michael`_, 2017. |2017 article name|_
.. #     |2017 journal name|_ *14*\ (134), p.20170425. :sup:`DOI:
.. #     10.1098/rsif.2017.0425`
.. # 
.. # .. # ------------------( LINKS ~ paper ~ 2016               )------------------
.. # .. _2016 article:
.. #    http://journal.frontiersin.org/article/10.3389/fbioe.2016.00055/abstract
.. # 
.. # .. |2016 article name| replace::
.. #    **Exploring instructive physiological signaling with the bioelectric tissue
.. #    simulation engine (BETSE).**
.. # .. _2016 article name:
.. #    http://journal.frontiersin.org/article/10.3389/fbioe.2016.00055/abstract
.. # 
.. # .. |2016 journal name| replace::
.. #    *Frontiers in Bioengineering and Biotechnology,*
.. # .. _2016 journal name:
.. #    http://journal.frontiersin.org/journal/bioengineering-and-biotechnology
.. # 
.. # .. # ------------------( LINKS ~ paper ~ 2017               )------------------
.. # .. |2017 article name| replace::
.. #    **Bioelectric gene and reaction networks: computational modelling of genetic, biochemical and bioelectrical dynamics in pattern regulation.**
.. # .. _2017 article name:
.. #    http://rsif.royalsocietypublishing.org/content/14/134/20170425
.. # 
.. # .. |2017 journal name| replace::
.. #    *Journal of The Royal Society Interface,*
.. # .. _2017 journal name:
.. #    http://rsif.royalsocietypublishing.org

Authors
=======

BENN comes courtesy a dedicated community of `authors <author list_>`__ and
contributors_ – without whom this project would be computationally impoverished,
biologically misaligned, and simply unusable.

**Thanks, all.**

.. # ------------------( LINKS ~ betse                      )------------------
.. _BETSE:
   https://gitlab.com/betse/betse
.. _BETSE live:
   https://gitlab.com/betse/betse#advanced

.. # ------------------( LINKS ~ benn                     )------------------
.. _author list:
   doc/rst/AUTHORS.rst
.. _codebase:
   https://gitlab.com/betse/benn/tree/master
.. _contributors:
   https://gitlab.com/betse/benn/graphs/master
.. _dependencies:
   doc/md/INSTALL.md
.. _testing:
   https://gitlab.com/betse/benn/pipelines
.. _tarballs:
   https://gitlab.com/betse/benn/tags
.. _Ubuntu 16.04 installer:
   https://gitlab.com/betse/benn/blob/master/bin/install/linux/benn_ubuntu_16_04.bash

.. # ------------------( LINKS ~ academia                   )------------------
.. _Pietak, Alexis:
   https://www.researchgate.net/profile/Alexis_Pietak
.. _Levin, Michael:
   https://ase.tufts.edu/biology/labs/levin
.. _Paul Allen Discovery Center:
   http://www.alleninstitute.org/what-we-do/frontiers-group/discovery-centers/allen-discovery-center-tufts-university
.. _Paul Allen Discovery Center award:
   https://www.alleninstitute.org/what-we-do/frontiers-group/news-press/press-resources/press-releases/paul-g-allen-frontiers-group-announces-allen-discovery-center-tufts-university
.. _Paul G. Allen Frontiers Group:
   https://www.alleninstitute.org/what-we-do/frontiers-group
.. _Tufts University:
   https://www.tufts.edu

.. # ------------------( LINKS ~ science                    )------------------
.. _biochemical reaction networks:
   http://www.nature.com/subjects/biochemical-reaction-networks
.. _electrodiffusion:
   https://en.wikipedia.org/wiki/Nernst%E2%80%93Planck_equation
.. _electro-osmosis:
   https://en.wikipedia.org/wiki/Electro-osmosis
.. _finite volume:
   https://en.wikipedia.org/wiki/Finite_volume_method
.. _galvanotaxis:
   https://en.wiktionary.org/wiki/galvanotaxis
.. _gene regulatory networks:
   https://en.wikipedia.org/wiki/Gene_regulatory_network
.. _voltage-gated ion channels:
   https://en.wikipedia.org/wiki/Voltage-gated_ion_channel

.. # ------------------( LINKS ~ software                   )------------------
.. _Anaconda:
   https://www.continuum.io/downloads
.. _Appveyor:
   https://ci.appveyor.com/project/betse/betse/branch/master
.. _Bash on Ubuntu on Windows:
   http://www.windowscentral.com/how-install-bash-shell-command-line-windows-10
.. _FFmpeg:
   https://ffmpeg.org
.. _Git:
   https://git-scm.com/downloads
.. _GitLab-CI:
   https://about.gitlab.com/gitlab-ci
.. _Graphviz:
   http://www.graphviz.org
.. _Homebrew:
   http://brew.sh
.. _Libav:
   https://libav.org
.. _macOS:
   https://en.wikipedia.org/wiki/Macintosh_operating_systems
.. _MacPorts:
   https://www.macports.org
.. _Matplotlib:
   http://matplotlib.org
.. _Miniconda:
   https://conda.io/miniconda.html
.. _NumPy:
   http://www.numpy.org
.. _MEncoder:
   https://en.wikipedia.org/wiki/MEncoder
.. _POSIX:
   https://en.wikipedia.org/wiki/POSIX
.. _PPA:
   https://launchpad.net/ubuntu/+ppas
.. _PyPI:
   https://pypi.python.org
.. _Python:
   https://www.python.org
.. _py.test:
   http://pytest.org
.. _SciPy:
   http://www.scipy.org
.. _YAML:
   http://yaml.org

.. # ------------------( LINKS ~ software : linux           )------------------
.. _APT:
   https://en.wikipedia.org/wiki/Advanced_Packaging_Tool
.. _Arch Linux:
   https://www.archlinux.org
.. _CentOS:
   https://www.centos.org
.. _Gentoo Linux:
   https://gentoo.org
.. _Ubuntu:
.. _Ubuntu Linux:
   https://www.ubuntu.com
.. _Ubuntu Linux 16.04 (Xenial Xerus):
   http://releases.ubuntu.com/16.04

.. # ------------------( LINKS ~ software : licenses        )------------------
.. _BSD 2-clause license:
   https://opensource.org/licenses/BSD-2-Clause
